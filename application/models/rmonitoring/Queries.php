<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Queries extends CI_Model {
    //put your code here
    function __construct(){
        parent::__construct();
    }    
    
    function logout_user() {
        $this->db->delete('ci_sessions', array('id' => $this->input->post('id')));
        return true;        
    }
}
