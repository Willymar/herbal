<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Queries extends CI_Model {
    //put your code here
    function __construct(){
        parent::__construct();
    }    
    function all_data() {
        $query = $this->db->get('penyakit');
        return $query->num_rows();
    }
    
    function data_pengguna($limit,$offset,$ordercol = 'idp',$orderby = 'DESC'){
        $this->db->order_by($ordercol,$orderby);
        $this->db->limit($limit,$offset);
        $query = $this->db->get('penyakit');
        return $query->result();
    } 
    
    function select_data($id = NULL) {
        if(!empty($id)){
            $this->db->where('idp', $id);
        }        
        $query = $this->db->get('penyakit');
        return $query->result();   
    }
    
    function save_data() {
        $data = array(
            'namapny' => $this->input->post('namapenyakit'),
            'jenispny' => $this->input->post('jenispenyakit'),
            'ket' => $this->input->post('keterangan'),            
        );
        $this->db->insert('penyakit', $data);
        return true;        
    }
    
    function update_data() {
        $data = array(
            'namapny' => $this->input->post('namapenyakit'),
            'jenispny' => $this->input->post('jenispenyakit'),
            'ket' => $this->input->post('keterangan'),            
        );
        $this->db->where('idp', $this->input->post('idp'));
        $this->db->update('penyakit', $data);
        return true;        
    }
    
    function delete_data() {
        $this->db->delete('penyakit', array('idp' => $this->input->post('idp')));
        return true;        
    }
}
