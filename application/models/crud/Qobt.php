<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Qobt extends CI_Model {
    //put your code here
    function __construct(){
        parent::__construct();
    }    
    function all_data() {
        $query = $this->db->get('obat');
        return $query->num_rows();
    }
    
    function data_pengguna($limit,$offset,$ordercol = 'ido',$orderby = 'DESC'){
        $this->db->order_by($ordercol,$orderby);
        $this->db->limit($limit,$offset);
        $query = $this->db->get('obat');
        return $query->result();
    } 
    
    function select_data($id = NULL) {
        if(!empty($id)){
            $this->db->where('ido', $id);
        }        
        $query = $this->db->get('obat');
        return $query->result();   
    }
    
    function save_data() {
        $data = array(
            'namaobt' => $this->input->post('namaobat'),
            'khasiat' => $this->input->post('khasiat'),
            'saran' => $this->input->post('saran'),  
            'carapgn' => $this->input->post('carapenggunaan'),            
        );
        $this->db->insert('obat', $data);
        return true;        
    }
    
    function update_data() {
        $data = array(
            'namaobt' => $this->input->post('namaobat'),
            'khasiat' => $this->input->post('khasiat'),
            'saran' => $this->input->post('saran'),
            'carapgn' => $this->input->post('carapenggunaan'),              
        );
        $this->db->where('ido', $this->input->post('ido'));
        $this->db->update('obat', $data);
        return true;        
    }
    
    function delete_data() {
        $this->db->delete('obat', array('ido' => $this->input->post('ido')));
        return true;        
    }
}
