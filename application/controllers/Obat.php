<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Obat extends CI_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library(array('form_validation','pagination'));
    }
    
    public function index() {
        $logged_in = $this->session->userdata('logged_in');
        if(!$logged_in){
            header("location: ".base_url());
        }
        
        $this->load->model('crud/qobt');
        $hal = number_format($this->uri->segment(3));
        $per_page = 10;
        
        $pcfg = array(
            'base_url' => $this->links->get_link() . '/index/',
            'per_page' => $per_page,
            'total_rows' => $this->qobt->all_data(),
            'attributes' => array('class' => 'btn btn-default'),
            'full_tag_open' => '<div class="btn-group">',
            'full_tag_close' => '</div>',
            'cur_tag_open' => '<button type="button" class="btn btn-danger">',
            'cur_tag_close' => '</button>',
            'first_link' => 'Awal',
            'last_link' => 'Akhir',
        );
        
        $this->pagination->initialize($pcfg);

        $res = array(
            'data' => $this->qobt->data_pengguna($per_page,$hal),
            'jmldata' => $pcfg['total_rows'],
        );
        
        $this->load->view('design/header');
        $this->load->view('obat/index',$res);
        $this->load->view('design/footer');
    }
    
    public function add() {
        $this->load->view('design/header');
        $this->load->view('obat/add');
        $this->load->view('design/footer');
    }
    
    public function edit() {
        $id = $this->uri->segment(3);
        if(empty($id)){
            redirect('obat/add');
        }
        
        $this->load->model('crud/qobt');
        $res['data'] = $this->qobt->select_data($id);
        
        if(empty($res['data'])){
            redirect('obat/add');
        }
        
        $this->load->view('design/header');
        $this->load->view('obat/edit',$res);
        $this->load->view('design/footer');
    }
    
    public function validate() {
        $config = array(
                array(
                        'field' => 'namaobat',
                        'label' => 'Nama Obat',
                        'rules' => 'required',
                        'errors' => array( 'required' => 'Nama Obat harus diisi', ),
                ),
                array(
                        'field' => 'khasiat',
                        'label' => 'Khasiat Obat',
                        'rules' => 'required',
                        'errors' => array( 'required' => 'Khasiat obat wajib diisi', ),
                ),
                array(
                        'field' => 'saran',
                        'label' => 'Saran',
                        'rules' => 'required',
                        'errors' => array( 'required' => 'Saran terhadap obat harus diisi', ),
                ),
                array(
                        'field' => 'carapenggunaan',
                        'label' => 'Cara Penggunaan Obat',
                        'rules' => 'required',
                        'errors' => array( 'required' => 'Cara Menggunakan obat harus diisi', ),
                )                
                
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
    
    public function save() {
        if($this->validate() == true){
            $this->load->model('crud/qobt');
            $res = $this->qobt->save_data();
            if($res){
                redirect('obat');
            }
        }else{
            $this->load->view('design/header');
            $this->load->view('crud/add');
            $this->load->view('design/footer');
        }
    }
    
    public function update() {
        $id = $this->uri->segment(3);
        if($this->validate() == TRUE){
            $this->load->model('crud/qobt');
            $res = $this->qobt->update_data();
            if($res){
                redirect('obat');
            }
        }else{
            redirect('obat/edit/' . $id);
        }
    }
    
    public function delete() {
        $id = $this->uri->segment(3);
        if(empty($id)){
            redirect('obat');
        }
        
        if($this->input->post('ido')){
            $this->load->model('crud/qobt');
            $res = $this->qobt->delete_data();
            if($res){
                redirect('obat');
            }
        }
        
        $this->load->model('crud/qobt');
        $res['data'] = $this->qobt->select_data($id);
        
        if(empty($res['data'])){
            redirect('obat');
        }
        
        $this->load->view('design/header');
        $this->load->view('obat/delete',$res);
        $this->load->view('design/footer');        
    }
}