    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>Tambah Data <span class="semi-bold">Penyakit</span></h3>
      </div>
    <!-- BEGIN BASIC FORM ELEMENTS-->
    
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4>Form <span class="semi-bold">Tambah</span></h4>
                  
                </div>
                <div class="grid-body no-border"> <br>
                  <div class="row">
                   <?php
                      $attributes = array('role' => 'form', 'id' => 'formadd');
                      echo form_open('crud/save',$attributes); 
                      ?>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                    <?php 
                    echo validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>','</div>');
                    ?>
                      <input type="hidden" name="idp" />
                      <div class="form-group">
                        <label class="form-label">Nama penyakit</label>
                        <span class="help">e.g. "Ejakulasi Dini"</span>
                        <div class="controls">
                          <input type="text" name="namapenyakit" id="namapenyakit" class="form-control" value="<?php echo set_value('namalengkap'); ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-label">Jenis penyakit</label>
                        <span class="help">e.g. "Mainstream"</span>
                        <div class="controls">
                        <select name="jenispenyakit" id="jenispenyakit" class="form-control" required>
                          <option value="" <?php echo set_select('jenispenyakit', '', TRUE); ?>>-- Pilih Jenis Penyakit --</option>
                          <option value="tidakmenular" <?php echo set_select('jenispenyakit', 'tidakmenular'); ?>>Tidak Menular</option>
                          <option value="menular" <?php echo set_select('jenispenyakit', 'menular'); ?>>Menular</option>                          
                          <option value="langka" <?php echo set_select('jenispenyakit', 'langka'); ?>>Langka</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-label">Keterangan</label>                      
                        <div class="controls">
                          <textarea id="keterangan" name="keterangan" class="form-control"><?php echo set_value('keterangan'); ?></textarea>
                        </div>
                      </div>
                      <button class="btn btn-primary">Simpan</button>
                      <a href="<?=base_url() . 'crud';?>" class="btn btn-default">Batal</a>                      
                    </div>

                    <div class="col-md-4">
                      <h3>Pilih <span class="semi-bold">Obat Herbal</span></h3>
                      <p>Pilih Salah satu obat herbal yang dapat menyembuhkan penyakit yang akan diinput</p>
                      <br>
                      <select id="multi" style="width:100%" multiple>
                        <option value="Jim">Jim</option>
                        <option value="John">John</option>
                        <option value="Lucy">Lucy</option>
                      </select>
                    </div>

                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>

  <!-- END BASIC FORM ELEMENTS-->  