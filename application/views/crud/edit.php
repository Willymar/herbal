    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>Edit Data <span class="semi-bold">Penyakit</span></h3>
      </div>
      <!-- BEGIN BASIC FORM ELEMENTS-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="grid simple">
            <div class="grid-title no-border">
              <h4>Form <span class="semi-bold">Edit</span></h4>
              
            </div>
            <div class="grid-body no-border"> <br>
              <div class="row">
               <?php
               $id = $this->uri->segment(3);
               $attributes = array('role' => 'form', 'id' => 'formedit');
               echo form_open('crud/update/'.$id,$attributes); 
               ?>

               <div class="col-md-8 col-sm-8 col-xs-8">
                <?php 
                echo validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>','</div>');
                
                foreach ($data as $val) {
                  $namapenyakit = $val->namapny;
                  $jenispenyakit = $val->jenispny;
                  $keterangan = $val->ket;
                   }
                ?>
                  <input type="hidden" name="idp" value="<?php echo $id;?>" />
                  <div class="form-group">
                    <label class="form-label">Nama penyakit</label>
                    <span class="help">e.g. "Ejakulasi Dini"</span>
                    <div class="controls">
                      <input type="text" class="form-control" value="<?php echo $namapenyakit; ?>" id="namapenyakit" name="namapenyakit">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="form-label">Jenis penyakit</label>
                    <span class="help">e.g. "Mainstream"</span>
                    <div class="controls">
                      <select name="jenispenyakit" id="jenispenyakit" class="form-control" required="required">
                      <option value="" <?php echo set_select('jenispenyakit', ''); ?>>-- Pilih Jenis Kelamin --</option>
                        <option value="tidakmenular" <?php if(!strcmp('tidakmenular',$jenispenyakit)){echo "selected=selected";}?>>Tidak Menular</option>
                        <option value="menular" <?php if(!strcmp('menular',$jenispenyakit)){echo "selected=selected";}?>>Menular</option>                          
                        <option value="langka" <?php if(!strcmp('langka',$jenispenyakit)){echo "selected=selected";}?>>Langka</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="form-label" >Keterangan</label>                      
                    <div class="controls">
                      <textarea class="form-control" id="keterangan" name="keterangan"><?php echo $keterangan; ?></textarea>
                    </div>
                  </div>
                  <button class="btn btn-primary">Simpan</button>
                  <a href="<?=base_url() . 'crud';?>" class="btn btn-default">Batal</a>
                </div>
                <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- END BASIC FORM ELEMENTS-->  