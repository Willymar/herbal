  <div class="content">
    <ul class="breadcrumb">
      <li>
        <p>Penyakit</p>
      </li>
      <li><a href="#" class="active">Tables</a> </li>
    </ul>
    <div class="page-title"> <i class="icon-custom-left"></i>
      <h3>Daftar - <span class="semi-bold">Penyakit</span></h3>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple ">
          <div class="grid-title">          
            <li class="m-r-10 input-prepend inside search-form no-boarder"> <span class="add-on"> <span class="iconset top-search"></span></span>
              <input name="" type="text"  class="no-boarder " placeholder="Search Penyakit" style="width:250px;">
            </li>
            <div class="tools">
             <button type="button" onclick="window.location.href='<?=base_url() . 'crud/add';?>'" class="btn btn-primary"><i class="fa fa-check"></i><a href="<?=base_url() . 'crud/add';?>"></a>&nbsp;Add New</button>

             <!-- <a href="<?=base_url() . 'crud/add';?>" class="btn btn-primary btn-cons" role="button"><i class="fa fa-check"></i>Tambah</a> -->
           </div>
         </div>
         <div class="grid-body ">
          <table class="table table-striped" id="example2" >
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Penyakit</th>
                <th>Jenis Penyakit</th>
                <th>Keterangan</th>                
              </tr>
            </thead>
            <tbody>

              <?php foreach ($data as $key){
                echo "             
                <tr >
                  <td>".$key->idp."</td>
                  <td>".$key->namapny."</td>
                  <td>".$key->jenispny."</td>
                  <td>".$key->ket."</td>                 
                  <td><div class=\"btn-group\">
                    <button class=\"btn btn-mini btn-success btn-demo-space\">Action</button>
                    <button class=\"btn btn-mini btn-success dropdown-toggle btn-demo-space\" data-toggle=\"dropdown\"> <span class=\"caret\"></span> </button>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"".base_url()."crud/edit/".$key->idp."\">Edit</a></li>
                      <li><a href=\"".base_url()."crud/delete/".$key->idp."\">Delete</a></li>                          
                      <li class=\"divider\"></li>
                      <li><a href=\"".base_url()."crud/report/".$key->idp."\">Details</a></li>
                    </ul>
                  </div></td>                   
                </tr>";
              } ?>
            </tbody>
          </table>
          <div class="row">
            <div class="col-lg-12" align="right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>

</div>
