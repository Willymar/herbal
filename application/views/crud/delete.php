<div class="content">
        <?php
        $id = $this->uri->segment(3);
        $attributes = array('role' => 'form', 'idp' => 'formedit');
        $hidden = array('idp' => $id);
        echo form_open('crud/delete/'.$id,$attributes,$hidden); 
        foreach ($data as $key) {
            echo "<p>Yakin akan menghapus data " . $key->namapny . " ?</p>"; 
        }
        echo form_submit('submit', 'Ya!','class="btn btn-primary"');
        ?>
        <a href="<?=base_url() . 'crud';?>" class="btn btn-default">Tidak</a>
        <?php echo form_close();  ?>
</div>