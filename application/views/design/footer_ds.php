<!-- BEGIN CORE JS FRAMEWORK-->
<script src="<?=base_url();?>assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/breakpoints.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="<?=base_url();?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-ricksaw-chart/js/raphael-min.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-ricksaw-chart/js/d3.v2.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-morris-chart/js/morris.min.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-easy-pie-chart/js/jquery.easypiechart.min.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-jvectormap/js/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-jvectormap/js/jquery-jvectormap-us-lcc-en.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/plugins/jquery-sparkline/jquery-sparkline.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-flot/jquery.flot.min.js"></script>
<script src="<?=base_url();?>assets/plugins/jquery-flot/jquery.flot.animator.min.js"></script>
<script src="<?=base_url();?>assets/plugins/skycons/skycons.js"></script>
<!-- END PAGE LEVEL PLUGINS   -->
<!-- PAGE JS -->
<script src="<?=base_url();?>assets/js/dashboard.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<script src="<?=base_url();?>assets/js/core.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/chat.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/demo.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->
</body>
</html>
