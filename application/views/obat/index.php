  <div class="content">
    <ul class="breadcrumb">
      <li>
        <p>Penyakit</p>
      </li>
      <li><a href="#" class="active">Tables</a> </li>
    </ul>
    <div class="page-title"> <i class="icon-custom-left"></i>
      <h3>Daftar - <span class="semi-bold">Obat Herbal</span></h3>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple ">
          <div class="grid-title">          
            <li class="m-r-10 input-prepend inside search-form no-boarder"> <span class="add-on"> <span class="iconset top-search"></span></span>
              <input name="" type="text"  class="no-boarder " placeholder="Search Penyakit" style="width:250px;">
            </li>
            <div class="tools">
             <button type="button" onclick="window.location.href='<?=base_url() . 'obat/add';?>'" class="btn btn-primary"><i class="fa fa-check"></i><a href="<?=base_url() . 'obat/add';?>"></a>&nbsp;Add New</button>

           </div>
         </div>
         <div class="grid-body ">
          <table class="table table-striped" id="example2" >
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Obat</th>
                <th>Khasiat</th>
                <th>Saran</th>
                <th>Cara Penggunaan</th>                 
              </tr>
            </thead>
            <tbody>

              <?php foreach ($data as $key){
                echo "             
                <tr >
                  <td>".$key->ido."</td>
                  <td>".$key->namaobt."</td>
                  <td>".$key->khasiat."</td>
                  <td>".$key->saran."</td>
                  <td>".$key->carapgn."</td>                 
                  <td><div class=\"btn-group\">
                    <button class=\"btn btn-mini btn-success btn-demo-space\">Action</button>
                    <button class=\"btn btn-mini btn-success dropdown-toggle btn-demo-space\" data-toggle=\"dropdown\"> <span class=\"caret\"></span> </button>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"".base_url()."obat/edit/".$key->ido."\">Edit</a></li>
                      <li><a href=\"".base_url()."obat/delete/".$key->ido."\">Delete</a></li>                          
                      <li class=\"divider\"></li>
                      <li><a href=\"".base_url()."obat/report/".$key->ido."\">Details</a></li>
                    </ul>
                  </div></td>                   
                </tr>";
              } ?>
            </tbody>
          </table>
          <div class="row">
            <div class="col-lg-12" align="right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>

</div>
