<div class="content">
        <?php
        $id = $this->uri->segment(3);
        $attributes = array('role' => 'form', 'ido' => 'formedit');
        $hidden = array('ido' => $id);
        echo form_open('obat/delete/'.$id,$attributes,$hidden); 
        foreach ($data as $key) {
            echo "<p>Yakin akan menghapus data " . $key->namaobt . " ?</p>"; 
        }
        echo form_submit('submit', 'Ya!','class="btn btn-primary"');
        ?>
        <a href="<?=base_url() . 'obat';?>" class="btn btn-default">Tidak</a>
        <?php echo form_close();  ?>
</div>