    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>Tambah Data <span class="semi-bold">Obat Herbal</span></h3>
      </div>
    <!-- BEGIN BASIC FORM ELEMENTS-->
    
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4>Form <span class="semi-bold">Tambah</span></h4>
                  
                </div>
                <div class="grid-body no-border"> <br>
                  <div class="row">
                   <?php
                      $attributes = array('role' => 'form', 'id' => 'formadd');
                      echo form_open('obat/save',$attributes); 
                      ?>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                    <?php 
                    echo validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button>','</div>');
                    ?>
                      <input type="hidden" name="ido" />
                      <div class="form-group">
                        <label class="form-label">Nama Obat</label>                       
                        <div class="controls">
                          <input type="text" name="namaobat" id="namaobat" class="form-control" placeholder="Masukkan nama obat herbal" value="<?php echo set_value('namaobat'); ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="form-label">Khasiat obat</label>                        
                        <div class="controls">
                          <input type="text" name="khasiat" id="khasiat" class="form-control" placeholder="Masukkan khasiat obat" value="<?php echo set_value('khasiat'); ?>">
                        </div>
                      </div>

                     <div class="form-group">
                        <label class="form-label">Saran</label>                      
                        <div class="controls">
                          <textarea id="saran" name="saran" placeholder="Saran atau Efek samping Obat" class="form-control"><?php echo set_value('saran'); ?></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="form-label">Cara Penggunaan</label>                      
                        <div class="controls">
                          <textarea id="carapenggunaan" name="carapenggunaan" placeholder="Cara penggunaan obat herbal" class="form-control"><?php echo set_value('carapenggunaan'); ?></textarea>
                        </div>
                      </div>

                      <button class="btn btn-primary">Simpan</button>
                      <a href="<?=base_url() . 'obat';?>" class="btn btn-default">Batal</a>
                      
                    </div>

                    <div class="col-md-4">
                      <div class="grid simple">
                        <div class="grid-title no-border">
                          <h4>Drag n Drop <span class="semi-bold">Uploader</span></h4>                          
                        </div>
                        <div class="grid-body no-border">
                          <div class="row-fluid">
                            <form role="form" name="uploadfotopasien" id="uploadfotopasien" 
                            action="javascript:simpanfoto();" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                              <h3>
                                <label class="label label-info msg">
                                  Silahkan pilih file
                                </label>                            
                              </h3>
                              <input id="userfile" name="userfile" type="file">
                            </div>
                            <button type="submit" onclick="javascript:simpanfoto();" class="btn btn-primary">
                              <span class="glyphicon glyphicon-plus"></span>&nbsp;Upload
                            </button>
                            <div class="form-group">
                              <div class="hasil"></div>
                            </div>
                          </form>
                            </div>
                          </div>
                        </div>
                      </div>

                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>

  <!-- END BASIC FORM ELEMENTS-->  