    <div class="content">
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>Edit Data <span class="semi-bold">Penyakit</span></h3>
      </div>
      <!-- BEGIN BASIC FORM ELEMENTS-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="grid simple">
            <div class="grid-title no-border">
              <h4>Form <span class="semi-bold">Edit</span></h4>
              
            </div>
            <div class="grid-body no-border"> <br>
              <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-8">


                  <div class="form-group">
                    <label class="form-label">Nama penyakit</label>
                    <span class="help">e.g. "Ejakulasi Dini"</span>
                    <div class="controls">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="form-label">Jenis penyakit</label>
                    <span class="help">e.g. "Mainstream"</span>
                    <div class="controls">
                      <select name="jenis" id="jenis" class="form-control" required="required">
                        <option value="tidakmenular">Tidak Menular</option>
                        <option value="menular">Menular</option>                          
                        <option value="langka">Langka</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="form-label">Keterangan</label>                      
                    <div class="controls">
                      <textarea class="form-control"></textarea>
                    </div>
                  </div>
                  <button class="btn btn-primary">Submit</button>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- END BASIC FORM ELEMENTS-->  