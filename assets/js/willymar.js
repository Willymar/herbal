/* Webarch Admin Dashboard 
/* This JS is only for DEMO Purposes - Extract the code that you need
-----------------------------------------------------------------*/	
//Cool ios7 switch - Beta version
//Done using pure Javascript
$(document).ready(function(){
	  //Dropdown menu - select2 plug-in
	  $("#source").select2();
	  
	  //Multiselect - Select2 plug-in
	  $("#multi").val(["Jim","Lucy"]).select2();
	  
	
});
      
function simpanfoto(){
    var userfile=$('#userfile').val();
    
    $('#uploadfotopasien').ajaxForm({
     url:'<?php echo base_url("uploader/upload_file"); ?>',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html('Silahkan Tunggu ... ');
     },
     complete: function(xhr) {
        $('.msg').html('Mengupload file selesai!');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
     },
    });     
};
